# Makefile
.SUFFIXES:

CXX:=$(CROSS_COMPILE)g++
CC:=$(CROSS_COMPILE)gcc
EXTRA_CFLAGS:=-I./include $(shell pkg-config libnl-genl-3.0 --cflags)
EXTRA_CXXFLAGS:=-std=c++17 -fconcepts -I./include $(shell pkg-config mydatakeeper --cflags)
EXTRA_LDFLAGS:=$(shell pkg-config mydatakeeper libnl-genl-3.0 --libs) -lreg

TARGET=mydatakeeper-access-point
OBJ_FILES=\
	mydatakeeper-access-point.o \
	src/nl80211_utils.o \
	src/regdb_utils.o \
	src/ipapi-proxy.o
INC_FILES=\
	include/nl80211.h \
	include/nl80211_utils.h \
	include/regdb.h \
	include/regdb_utils.h \
	include/reglib.h \
	include/ipapi-proxy.h
GEN_FILES=include/co.ipapi.h

TEST_TARGET=tests/mydatakeeper-access-point
TEST_OBJ_FILES=\
	mydatakeeper-access-point.o \
	tests/nl80211_stub.o \
	src/regdb_utils.o \
	src/ipapi-proxy.o

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) $(EXTRA_LDFLAGS)

check: $(TEST_TARGET)

$(TEST_TARGET): $(TEST_OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) $(EXTRA_LDFLAGS)

include/%.h:$(CROOT)/usr/share/dbus-1/interfaces/%.xml
	dbusxx-xml2cpp $< --proxy=$@

%.o:%.c $(INC_FILES)
	$(CC) -c $< -o $@ $(CFLAGS) $(EXTRA_CFLAGS)

%.o:%.cpp $(INC_FILES) $(GEN_FILES)
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(EXTRA_CXXFLAGS)

clean:
	rm -rf $(TARGET) $(OBJ_FILES) $(GEN_FILES) $(TEST_TARGET) $(TEST_OBJ_FILES)
