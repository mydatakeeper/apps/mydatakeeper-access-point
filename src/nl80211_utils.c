#include "nl80211_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <errno.h>

#include <netlink/genl/genl.h>
// #include <netlink/genl/family.h>
#include <netlink/genl/ctrl.h>
#include <netlink/msg.h>
#include <netlink/attr.h>

#include "nl80211.h"

struct nl80211_state {
    struct nl_sock *nl_sock;
    int nl80211_id;
};

static int nl80211_init(struct nl80211_state *state)
{
    int err;

    state->nl_sock = nl_socket_alloc();
    if (!state->nl_sock) {
        fprintf(stderr, "Failed to allocate netlink socket.\n");
        return -ENOMEM;
    }

    if (genl_connect(state->nl_sock)) {
        fprintf(stderr, "Failed to connect to generic netlink.\n");
        err = -ENOLINK;
        goto out_handle_destroy;
    }

    nl_socket_set_buffer_size(state->nl_sock, 8192, 8192);

    /* try to set NETLINK_EXT_ACK to 1, ignoring errors */
    err = 1;
    setsockopt(nl_socket_get_fd(state->nl_sock), SOL_NETLINK,
           NETLINK_EXT_ACK, &err, sizeof(err));

    state->nl80211_id = genl_ctrl_resolve(state->nl_sock, "nl80211");
    if (state->nl80211_id < 0) {
        fprintf(stderr, "nl80211 not found.\n");
        err = -ENOENT;
        goto out_handle_destroy;
    }

    return 0;

 out_handle_destroy:
    nl_socket_free(state->nl_sock);
    return err;
}

static void nl80211_cleanup(struct nl80211_state *state)
{
    nl_socket_free(state->nl_sock);
}

static int error_handler(struct sockaddr_nl *nla, struct nlmsgerr *err, void *arg)
{
    struct nlmsghdr *nlh = (struct nlmsghdr *)err - 1;
    int len = nlh->nlmsg_len;
    struct nlattr *attrs;
    struct nlattr *tb[NLMSGERR_ATTR_MAX + 1];
    int *ret = (int*)arg;
    int ack_len = sizeof(*nlh) + sizeof(int) + sizeof(*nlh);

    *ret = err->error;

    if (!(nlh->nlmsg_flags & NLM_F_ACK_TLVS))
        return NL_STOP;

    if (!(nlh->nlmsg_flags & NLM_F_CAPPED))
        ack_len += err->msg.nlmsg_len - sizeof(*nlh);

    if (len <= ack_len)
        return NL_STOP;

    attrs = (struct nlattr*)((unsigned char *)nlh + ack_len);
    len -= ack_len;

    nla_parse(tb, NLMSGERR_ATTR_MAX, attrs, len, NULL);
    if (tb[NLMSGERR_ATTR_MSG]) {
        len = strnlen((char *)nla_data(tb[NLMSGERR_ATTR_MSG]),
                  nla_len(tb[NLMSGERR_ATTR_MSG]));
        fprintf(stderr, "kernel reports: %*s\n", len,
            (char *)nla_data(tb[NLMSGERR_ATTR_MSG]));
    }

    return NL_STOP;
}

static int ack_handler(struct nl_msg *msg, void *arg)
{
    int *ret = (int*)arg;
    *ret = 0;
    return NL_STOP;
}

static int finish_handler(struct nl_msg *msg, void *arg)
{
    int *ret = (int*)arg;
    *ret = 0;
    return NL_SKIP;
}

static struct nla_policy freq_policy[NL80211_FREQUENCY_ATTR_MAX + 1] = {
    {},
    { type : NLA_U32 },
    { type : NLA_FLAG },
    { type : NLA_FLAG },
    { type : NLA_FLAG },
    { type : NLA_FLAG },
    { type : NLA_U32 },
};

static void init_phy_band_frequency(struct nlattr *nl_freq, uint32_t* freq)
{
    struct nlattr *tb_freq[NL80211_FREQUENCY_ATTR_MAX + 1];
    nla_parse(tb_freq, NL80211_FREQUENCY_ATTR_MAX, (struct nlattr*)nla_data(nl_freq),
          nla_len(nl_freq), freq_policy);

    if (!tb_freq[NL80211_FREQUENCY_ATTR_FREQ])
        return;
    *freq = nla_get_u32(tb_freq[NL80211_FREQUENCY_ATTR_FREQ]);
}

static void init_phy_band(
    struct nlattr *nl_band,
    struct nl80211_phy_band* band)
{
    struct nlattr *tb_band[NL80211_BAND_ATTR_MAX + 1];
    nla_parse(tb_band, NL80211_BAND_ATTR_MAX, (struct nlattr*)nla_data(nl_band),
          nla_len(nl_band), NULL);

    if (tb_band[NL80211_BAND_ATTR_HT_CAPA]) {
        band->has_ht_cap = 1;
        band->ht_cap = nla_get_u16(tb_band[NL80211_BAND_ATTR_HT_CAPA]);
    } else {
        band->has_ht_cap = 0;
        band->ht_cap = ~0;
    }

    if (tb_band[NL80211_BAND_ATTR_VHT_CAPA]) {
        band->has_vht_cap = 1;
        band->vht_cap = nla_get_u32(tb_band[NL80211_BAND_ATTR_VHT_CAPA]);
    } else {
        band->has_vht_cap = 0;
        band->vht_cap = 0;
    }

    if (tb_band[NL80211_BAND_ATTR_FREQS]) {
        struct nlattr *nl_freq = (struct nlattr *) nla_data(tb_band[NL80211_BAND_ATTR_FREQS]);
        int rem_freq = nla_len(tb_band[NL80211_BAND_ATTR_FREQS]);
        int i = 0;

        while (nla_ok(nl_freq, rem_freq)) {
            nl_freq = nla_next(nl_freq, &rem_freq);
            ++i;
        }
        band->freq_count = i;
        band->freqs = (uint32_t*)malloc(i * sizeof(uint32_t));

        nl_freq = (struct nlattr *) nla_data(tb_band[NL80211_BAND_ATTR_FREQS]);
        rem_freq = nla_len(tb_band[NL80211_BAND_ATTR_FREQS]);
        i = 0;
        while (nla_ok(nl_freq, rem_freq)) {
            init_phy_band_frequency(nl_freq, &(band->freqs[i]));
            nl_freq = nla_next(nl_freq, &rem_freq);
            ++i;
        }
    }
}

static int valid_handler(struct nl_msg *msg, void *arg)
{
    struct nl80211_phy *phy = (struct nl80211_phy*)arg;

    struct genlmsghdr *gnlh = (struct genlmsghdr*)nlmsg_data(nlmsg_hdr(msg));

    struct nlattr *tb_msg[NL80211_ATTR_MAX + 1];
    nla_parse(tb_msg, NL80211_ATTR_MAX, genlmsg_attrdata(gnlh, 0),
          genlmsg_attrlen(gnlh, 0), NULL);

    if (tb_msg[NL80211_ATTR_WIPHY])
        phy->id = nla_get_u32(tb_msg[NL80211_ATTR_WIPHY]);
    else
        phy->id = ~0;

    if (tb_msg[NL80211_ATTR_WIPHY_NAME])
        phy->name = nla_strdup(tb_msg[NL80211_ATTR_WIPHY_NAME]);
    else
        phy->name = NULL;

    if (tb_msg[NL80211_ATTR_WIPHY_BANDS]) {
        struct nlattr *nl_band = (struct nlattr *) nla_data(tb_msg[NL80211_ATTR_WIPHY_BANDS]);
        int rem_band = phy->band_count = nla_len(tb_msg[NL80211_ATTR_WIPHY_BANDS]);
        int i = 0;

        while (nla_ok(nl_band, rem_band)) {
            nl_band = nla_next(nl_band, &rem_band);
            ++i;
        }
        phy->band_count = i;
        phy->bands = (struct nl80211_phy_band*)malloc(i * sizeof(struct nl80211_phy_band));

        nl_band = (struct nlattr *) nla_data(tb_msg[NL80211_ATTR_WIPHY_BANDS]);
        rem_band = nla_len(tb_msg[NL80211_ATTR_WIPHY_BANDS]);
        i = 0;
        while (nla_ok(nl_band, rem_band)) {
            init_phy_band(nl_band, &(phy->bands[i]));
            nl_band = nla_next(nl_band, &rem_band);
            ++i;
        }
    }

    return NL_OK;
}

int get_phy_description(signed long long idx, struct nl80211_phy *phy)
{
    int err;
    struct nl_msg *msg;
    struct nl_cb *cb;
    struct nl80211_state state;

    err = nl80211_init(&state);
    if (err)
        return 1;

    msg = nlmsg_alloc();
    if (!msg) {
        fprintf(stderr, "failed to allocate netlink message\n");
        err = 2;
        goto alloc_err;
    }

    cb = nl_cb_alloc(NL_CB_DEFAULT);
    if (!cb) {
        fprintf(stderr, "failed to allocate netlink callbacks\n");
        err = 2;
        goto nl_error;
    }

    genlmsg_put(msg, 0, 0, state.nl80211_id, 0, 0, NL80211_CMD_GET_WIPHY, 0);

    NLA_PUT_U32(msg, NL80211_ATTR_WIPHY, idx);

    err = nl_send_auto_complete(state.nl_sock, msg);
    if (err < 0)
        goto nl_error;

    err = 1;

    nl_cb_err(cb, NL_CB_CUSTOM, error_handler, &err);
    nl_cb_set(cb, NL_CB_FINISH, NL_CB_CUSTOM, finish_handler, &err);
    nl_cb_set(cb, NL_CB_ACK, NL_CB_CUSTOM, ack_handler, &err);
    nl_cb_set(cb, NL_CB_VALID, NL_CB_CUSTOM, valid_handler, phy);

    while (err > 0)
        nl_recvmsgs(state.nl_sock, cb);
 nl_error:
    nl_cb_put(cb);
    nlmsg_free(msg);
alloc_err:
    nl80211_cleanup(&state);
    return err;
 nla_put_failure:
    fprintf(stderr, "building message failed\n");
    return 2;
}

#ifdef __cplusplus
}
#endif
