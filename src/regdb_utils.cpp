#include "regdb_utils.h"

#include <iostream>
#include <cassert>

extern "C" {
    #include <reglib/reglib.h>
    #include "nl80211_utils.h"
}

namespace {

nl80211_phy *phy = NULL;
nl80211_phy *get_phy()
{
    if (phy == NULL) {
        phy = (nl80211_phy*)malloc(sizeof(nl80211_phy));
        int err = get_phy_description(0, phy);
        if (err) {
            // TODO: memory leak?
            free(phy);
            phy = NULL;
        }
    }

    return phy;
}

bool bands_found = false;
list<nl80211_phy_band*> bands_2ghz;
list<nl80211_phy_band*> bands_5ghz;
void find_2ghz_and_5ghz_bands()
{
    nl80211_phy *phy = get_phy();
    list<string> modes;
    for (size_t i = 0; i < phy->band_count; ++i) {
        nl80211_phy_band *band = &(phy->bands[i]);
        bool is_2ghz = true;
        bool is_5ghz = true;
        for (size_t j= 0; j < band->freq_count; ++j) {
            freq_t freq = band->freqs[j];
            if (freq < 2412 || freq > 2484)
                is_2ghz = false;
            if (freq < 5150 || freq > 5875)
                is_5ghz = false;
        }
        if (is_2ghz) {
            bands_2ghz.push_back(band);
        }
        if (is_5ghz) {
            bands_5ghz.push_back(band);
        }
    }

    bands_found = true;
}

list<nl80211_phy_band*>& get_2ghz_bands()
{
    if (!bands_found) {
        find_2ghz_and_5ghz_bands();
    }
    return bands_2ghz;
}

list<nl80211_phy_band*>& get_5ghz_bands()
{
    if (!bands_found) {
        find_2ghz_and_5ghz_bands();
    }
    return bands_5ghz;
}

set<hw_mode_t> modes;
set<hw_mode_t>& get_supported_modes()
{
    if (modes.empty()) {
        for (const nl80211_phy_band* band: get_2ghz_bands()) {
            (void)(band);
            modes.insert("b");
            modes.insert("g");
        }
        for (const nl80211_phy_band* band: get_5ghz_bands()) {
            (void)(band);
            modes.insert("a");
        }
    }

    return modes;
}

set<ieee80211n_t> get_supported_ieee80211n(const hw_mode_t &mode)
{
    if (mode == "b") {
        return { false };
    }

    if (mode == "g") {
        for (const nl80211_phy_band* band: get_2ghz_bands()) {
            if (band->has_ht_cap &&
                band->ht_cap & HT_CAP_CHANNEL_WIDTH &&
                !(band->ht_cap & HT_CAP_40MHZ_INTOLERANT)) {
                return { false, true };
            }
        }
        return { false };
    }

    if (mode == "a") {
        for (const nl80211_phy_band* band: get_5ghz_bands()) {
            if (band->has_ht_cap &&
                band->ht_cap & HT_CAP_CHANNEL_WIDTH &&
                !(band->ht_cap & HT_CAP_40MHZ_INTOLERANT)) {
                return { false, true };
            }
        }
        return { false };
    }

    return { false };
}

set<ieee80211ac_t> get_supported_ieee80211ac(const hw_mode_t &mode)
{
    if (mode == "b" || mode == "g") {
        return { false };
    }

    if (mode == "a") {
        for (const nl80211_phy_band* band: get_5ghz_bands()) {
            if (band->has_vht_cap) {
                return { false, true };
            }
        }
        return { false };
    }

    return { false };
}

set<bandwidth_t> get_supported_bandwidth(const hw_mode_t &mode,
    const ieee80211n_t &ieee80211n, const ieee80211ac_t &ieee80211ac)
{
    set<bandwidth_t> bandwidths = {"20"};
    if (mode == "b") {
        // Nothing to do
    } else if (mode == "g") {
        if (ieee80211n)
            for (const nl80211_phy_band* band: get_2ghz_bands())
                if (band->has_ht_cap && (band->ht_cap >> 1) & 1)
                    bandwidths.insert("40");
    } else if (mode == "a") {
        if (ieee80211n || ieee80211ac) {
            for (const nl80211_phy_band* band: get_5ghz_bands()) {
                if (ieee80211n && band->has_ht_cap &&
                    band->ht_cap & HT_CAP_CHANNEL_WIDTH &&
                    !(band->ht_cap & HT_CAP_40MHZ_INTOLERANT))
                    bandwidths.insert("40");
                if (ieee80211ac && band->has_vht_cap) {
                    bandwidths.insert("80");
                    if ((band->vht_cap & VHT_CAP_SUPP_CHAN_WIDTH_MASK) == VHT_CAP_SUPP_CHAN_WIDTH_160MHZ) {
                        bandwidths.insert("160");
                    }
                    if ((band->vht_cap & VHT_CAP_SUPP_CHAN_WIDTH_MASK) == VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ) {
                        bandwidths.insert("160");
                        /* bandwidths.insert("80+80"); */
                    }
                }
            }
        }
    }

    return bandwidths;
}

set<freq_t> freqs_2ghz_20mhz = {2412,2417,2422,2427,2432,2437,2442,2447,2452,2457,2462,2467,2472,2484,};
set<freq_t> freqs_2ghz_40mhz = {2422,2427,2432,2437,2442,2447,2452,2457,2462,};
set<freq_t> freqs_5ghz_20mhz = {/*5160,*/5180,5200,5220,5240,5260,5280,5300,5320,5500,5520,5540,5560,5580,5600,5620,5640,5660,5680,5700,5720,5745,5765,5785,5805,5825,/*5845,5865,*/};
set<freq_t> freqs_5ghz_40mhz = {/*5170,*/5190,5230,5270,5310,5510,5550,5590,5630,5670,5710,5755,5795,};
set<freq_t> freqs_5ghz_80mhz = {5210,5290,5530,5610,5690,5775,};
set<freq_t> freqs_5ghz_160mhz = {5250,5570,};
set<freq_t> get_supported_freqs(const string &mode, const string &bandwidth)
{
    set<freq_t> freqs;

    if (mode == "b") {
        assert(bandwidth == "20");
        for (const nl80211_phy_band* band: get_2ghz_bands())
            for (size_t i = 0; i < band->freq_count; ++i)
                if (freqs_2ghz_20mhz.find(band->freqs[i]) != freqs_2ghz_20mhz.end())
                    freqs.insert(band->freqs[i]);
    } else if (mode == "g") {
        for (const nl80211_phy_band* band: get_2ghz_bands())
            for (size_t i = 0; i < band->freq_count; ++i)
                if (bandwidth == "20" && freqs_2ghz_20mhz.find(band->freqs[i]) != freqs_2ghz_20mhz.end())
                    freqs.insert(band->freqs[i]);
                else if (bandwidth == "40" && freqs_2ghz_40mhz.find(band->freqs[i]) != freqs_2ghz_40mhz.end())
                    freqs.insert(band->freqs[i]);
    } else if (mode == "a") {
        for (const nl80211_phy_band* band: get_5ghz_bands())
            for (size_t i = 0; i < band->freq_count; ++i)
                if (bandwidth == "20" && freqs_5ghz_20mhz.find(band->freqs[i]) != freqs_5ghz_20mhz.end())
                    freqs.insert(band->freqs[i]);
                else if (bandwidth == "40" && freqs_5ghz_40mhz.find(band->freqs[i]) != freqs_5ghz_40mhz.end())
                    freqs.insert(band->freqs[i]);
                else if (bandwidth == "80" && freqs_5ghz_80mhz.find(band->freqs[i]) != freqs_5ghz_80mhz.end())
                    freqs.insert(band->freqs[i]);
                else if (bandwidth == "160" && freqs_5ghz_160mhz.find(band->freqs[i]) != freqs_5ghz_160mhz.end())
                    freqs.insert(band->freqs[i]);
    }

    return freqs;
}

bool rules_found = false;
wireless_rules_t wireless_rules;

} /* anonymous namespace */

set<string> get_ht_capabilities(const hw_mode_t &mode, const bandwidth_t &bandwidth)
{
    set<string> result;
    list<nl80211_phy_band*> bands;
    if (mode == "b" || mode == "g")
        bands = get_2ghz_bands();
    else if (mode == "a")
        bands = get_5ghz_bands();

    for (const nl80211_phy_band* band: bands) {
        if (!band->has_ht_cap)
            continue;

        if (band->ht_cap & HT_CAP_LDPC)
            result.insert("[LDPC]");
        if (band->ht_cap & HT_CAP_CHANNEL_WIDTH)
            result.insert("[HT40+]");
        // Remove this option for now as it seems to break the hostapd process
        // if ((band->ht_cap & HT_CAP_SMPS_MASK) == HT_CAP_SMPS_STATIC)
        //     result.insert("[SMPS-STATIC]");
        // if ((band->ht_cap & HT_CAP_SMPS_MASK) == HT_CAP_SMPS_DYNAMIC)
        //     result.insert("[SMPS-DYNAMIC]");
        if (band->ht_cap & HT_CAP_GREEN_FIELD)
            result.insert("[GF]");
        if (band->ht_cap & HT_CAP_SHORT_GI20MHZ)
            result.insert("[SHORT-GI-20]");
        if (bandwidth != "20" && band->ht_cap & HT_CAP_SHORT_GI40MHZ)
            result.insert("[SHORT-GI-40]");
        if (band->ht_cap & HT_CAP_TX_STBC)
            result.insert("[TX-STBC]");
        if ((band->ht_cap & HT_CAP_RX_STBC_MASK) == HT_CAP_RX_STBC_1)
            result.insert("[RX-STBC1]");
        if ((band->ht_cap & HT_CAP_RX_STBC_MASK) == HT_CAP_RX_STBC_12)
            result.insert("[RX-STBC12]");
        if ((band->ht_cap & HT_CAP_RX_STBC_MASK) == HT_CAP_RX_STBC_123)
            result.insert("[RX-STBC123]");
        if (band->ht_cap & HT_CAP_DELAYED_BA)
            result.insert("[DELAYED-BA]");
        if (band->ht_cap & HT_CAP_MAX_AMSDU_7935)
            result.insert("[MAX-AMSDU-7935]");
        if (band->ht_cap & HT_CAP_DSSS_CCK40MHZ)
            result.insert("[DSSS_CCK-40]");
        if (band->ht_cap & HT_CAP_40MHZ_INTOLERANT)
            result.insert("[40-INTOLERANT]");
        if (band->ht_cap & HT_CAP_LSIG_TXOP_PROT)
            result.insert("[LSIG-TXOP-PROT]");
    }

    return result;
}

set<string> get_vht_capabilities(const bandwidth_t &bandwidth)
{
    set<string> result;
    for (const nl80211_phy_band* band: get_5ghz_bands()) {
        if (!band->has_vht_cap)
            continue;

        if ((band->vht_cap & VHT_CAP_MAX_MPDU_LENGTH_MASK) == VHT_CAP_MAX_MPDU_LENGTH_7991)
            result.insert("[MAX-MPDU-7991]");
        else if ((band->vht_cap & VHT_CAP_MAX_MPDU_LENGTH_MASK) == VHT_CAP_MAX_MPDU_LENGTH_11454)
            result.insert("[MAX-MPDU-11454]");
        if ((band->vht_cap & VHT_CAP_SUPP_CHAN_WIDTH_MASK) == VHT_CAP_SUPP_CHAN_WIDTH_160MHZ)
            result.insert("[VHT160]");
        if ((band->vht_cap & VHT_CAP_SUPP_CHAN_WIDTH_MASK) == VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ)
            result.insert("[VHT160-80PLUS80]");
        if (band->vht_cap & VHT_CAP_RXLDPC)
            result.insert("[RXLDPC]");
        if (band->vht_cap & VHT_CAP_SHORT_GI_80)
            result.insert("[SHORT-GI-80]");
        if (bandwidth != "80" && (band->vht_cap & VHT_CAP_SHORT_GI_160))
            result.insert("[SHORT-GI-160]");
        if (band->vht_cap & VHT_CAP_TXSTBC)
            result.insert("[TX-STBC-2BY1]");
        if ((band->vht_cap & VHT_CAP_RXSTBC_MASK) == VHT_CAP_RXSTBC_1)
            result.insert("[RX-STBC-1]");
        if ((band->vht_cap & VHT_CAP_RXSTBC_MASK) == VHT_CAP_RXSTBC_2)
            result.insert("[RX-STBC-12]");
        if ((band->vht_cap & VHT_CAP_RXSTBC_MASK) == VHT_CAP_RXSTBC_3)
            result.insert("[RX-STBC-123]");
        if ((band->vht_cap & VHT_CAP_RXSTBC_MASK) == VHT_CAP_RXSTBC_4)
            result.insert("[RX-STBC-1234]");
        if (band->vht_cap & VHT_CAP_SU_BEAMFORMER_CAPABLE)
            result.insert("[SU-BEAMFORMER]");
        if (band->vht_cap & VHT_CAP_SU_BEAMFORMEE_CAPABLE)
            result.insert("[SU-BEAMFORMEE]");
        if (band->vht_cap & VHT_CAP_BEAMFORMEE_STS_2)
            result.insert("[BF-ANTENNA-2]");
        if (band->vht_cap & VHT_CAP_BEAMFORMEE_STS_3)
            result.insert("[BF-ANTENNA-3]");
        if (band->vht_cap & VHT_CAP_BEAMFORMEE_STS_4)
            result.insert("[BF-ANTENNA-4]");
        if (band->vht_cap & VHT_CAP_SOUNDING_DIMENSION_2)
            result.insert("[SOUNDING-DIMENSION-2]");
        if (band->vht_cap & VHT_CAP_SOUNDING_DIMENSION_3)
            result.insert("[SOUNDING-DIMENSION-3]");
        if (band->vht_cap & VHT_CAP_SOUNDING_DIMENSION_4)
            result.insert("[SOUNDING-DIMENSION-4]");
        if (band->vht_cap & VHT_CAP_MU_BEAMFORMER_CAPABLE)
            result.insert("[MU-BEAMFORMER]");
        if (band->vht_cap & VHT_CAP_MU_BEAMFORMEE_CAPABLE)
            result.insert("[MU-BEAMFORMEE]");
        if (band->vht_cap & VHT_CAP_VHT_TXOP_PS)
            result.insert("[VHT-TXOP-PS]");
        if (band->vht_cap & VHT_CAP_HTC_VHT)
            result.insert("[HTC-VHT]");
        if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX)
            result.insert("[MAX-A-MPDU-LEN-EXP7]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_6)
            result.insert("[MAX-A-MPDU-LEN-EXP6]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_5)
            result.insert("[MAX-A-MPDU-LEN-EXP5]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_4)
            result.insert("[MAX-A-MPDU-LEN-EXP4]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_3)
            result.insert("[MAX-A-MPDU-LEN-EXP3]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_2)
            result.insert("[MAX-A-MPDU-LEN-EXP2]");
        else if (band->vht_cap & VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_1)
            result.insert("[MAX-A-MPDU-LEN-EXP1]");
        if (band->vht_cap & VHT_CAP_VHT_LINK_ADAPTATION_VHT_UNSOL_MFB)
            result.insert("[VHT-LINK-ADAPT2]");
        if (band->vht_cap & VHT_CAP_VHT_LINK_ADAPTATION_VHT_MRQ_MFB)
            result.insert("[VHT-LINK-ADAPT3]");
        if (band->vht_cap & VHT_CAP_RX_ANTENNA_PATTERN)
            result.insert("[RX-ANTENNA-PATTERN]");
        if (band->vht_cap & VHT_CAP_TX_ANTENNA_PATTERN)
            result.insert("[TX-ANTENNA-PATTERN]");
    }

    return result;
}

void add_channels(
    const struct ieee80211_regdomain *regdomain,
    hw_mode_t mode,
    ieee80211n_t ieee80211n,
    ieee80211ac_t ieee80211ac,
    bandwidth_t bandwidth,
    country3_t environment,
    ieee80211h_t dfs)
{
    for (uint32_t n = 0; n < regdomain->n_reg_rules; ++n) {
        const struct ieee80211_reg_rule reg_rule = regdomain->reg_rules[n];
        uint8_t bw = atoi(bandwidth.c_str());

        // Check start frequence is in range
        freq_t max_freq = mode == "a" ? REGLIB_MHZ_TO_KHZ(5875) : REGLIB_MHZ_TO_KHZ(2494);
        if (reg_rule.freq_range.start_freq_khz >= max_freq)
            continue;

        // Check end frequence is in range
        freq_t min_freq = mode == "a" ? REGLIB_MHZ_TO_KHZ(5150) : REGLIB_MHZ_TO_KHZ(2400);
        if (reg_rule.freq_range.end_freq_khz <= min_freq)
            continue;

        // Check bandwidth is in range
        if (reg_rule.freq_range.max_bandwidth_khz < REGLIB_MHZ_TO_KHZ(bw))
            continue;

        // Check if we can initiate radiation
        if (reg_rule.flags & RRF_NO_IR_ALL)
            continue;

        // Check if the environment is compatible
        if (reg_rule.flags & RRF_NO_OUTDOOR && environment == "0x4f")
            continue;

        // Check if the environment is compatible
        if (reg_rule.flags & RRF_NO_INDOOR && environment == "0x49")
            continue;

        // Check if the encoding is compatible
        if (reg_rule.flags & RRF_NO_OFDM && mode != "b")
            continue;

        // Check if the DFS support is requested
        if (reg_rule.flags & RRF_DFS && !dfs)
            continue;

        const ieee80211_freq_range& allowed = reg_rule.freq_range;
        for (freq_t supported: get_supported_freqs(mode, bandwidth)) {
            freq_t supported_start = REGLIB_MHZ_TO_KHZ(supported-(bw/2));
            freq_t supported_end = REGLIB_MHZ_TO_KHZ(supported+(bw/2));
            if (supported_start < allowed.start_freq_khz)
                continue;
            if (supported_end > allowed.end_freq_khz)
                continue;

            wireless_rules
                [country_t(regdomain->alpha2, 2)]
                [mode]
                [ieee80211n]
                [ieee80211ac]
                [bandwidth]
                [environment]
                [dfs]
            .push_back(supported);
        }
    }
}

wireless_rules_t& get_wireless_rules()
{
    if (rules_found)
        return wireless_rules;

    const struct reglib_regdb_ctx *ctx = reglib_malloc_regdb_ctx(REGBD_FILENAME);
    if (!ctx) {
        cerr << "Invalid or empty regulatory file" << endl;
        return wireless_rules;
    }

    const struct ieee80211_regdomain *rd = NULL;
    unsigned int idx = 0;
    reglib_for_each_country(rd, idx, ctx) {
        if (!reglib_is_valid_rd(rd)) {
            free((struct ieee80211_regdomain *) rd);
            continue;
        }

        for (hw_mode_t mode: get_supported_modes()) {
            for (ieee80211n_t ieee80211n: get_supported_ieee80211n(mode)) {
                for (ieee80211ac_t ieee80211ac: get_supported_ieee80211ac(mode)) {
                    for (bandwidth_t bw: get_supported_bandwidth(mode, ieee80211n, ieee80211ac)) {
                        static list<country3_t> ieee80211_environment = {"0x4f", "0x49"};
                        for (country3_t env: ieee80211_environment) {
                            static list<ieee80211h_t> ieee80211h = {false, true};
                            for (ieee80211h_t dfs: ieee80211h) {
                                if (mode != "a" && dfs == true)
                                    continue;

                                add_channels(
                                    rd,
                                    mode,
                                    ieee80211n,
                                    ieee80211ac,
                                    bw,
                                    env,
                                    dfs
                                );
                            }
                        }
                    }
                }
            }
        }
        free((struct ieee80211_regdomain *) rd);
    }
    reglib_free_regdb_ctx(ctx);

    rules_found = true;
    return wireless_rules;
}
