#include <fstream>
#include <iostream>
#include <chrono>
#include <thread>
#include <filesystem>

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include "regdb_utils.h"
#include "ipapi-proxy.h"

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>
#include <mydatakeeper/application.h>
#include <mydatakeeper/systemd.h>

using namespace std;

// Child PID
pid_t pid;

// HostAPd config files
string config_file;
string config_blacklist;
string config_whitelist;

DBus::BusDispatcher dispatcher;

void killIfForked()
{
    if (pid != 0) {
        clog << "Killing hostapd process" << endl;
        if (kill(pid, SIGTERM) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        join(pid);
        pid = 0;
    } else {
        clog << "No hostapd process to kill" << endl;
    }
}

void forkOrRestart()
{
    killIfForked();
    clog << "Forking and launching hostapd process" << endl;
    pid = execute_async("/usr/bin/hostapd", {
        "-P", "/run/hostapd/hostapd.pid",
        "-i", "wlan0",
        config_file,
    });
    if (pid == -1) {
        exit(1);
    }
    clog << "hostapd process has PID " << pid << endl;

    // BEGIN : ugly hack to fix systemd-networkd status
    // https://github.com/systemd/systemd/issues/936
    while (true) {
        if (filesystem::exists("/run/hostapd/wlan0"))
            break;
        if (waitpid(pid, nullptr, WNOHANG) != 0)
            exit(1);
        this_thread::sleep_for(chrono::seconds(1));
    }
    clog << "Reconfigure systemd-networkd wlan0" << endl;
    execute("/usr/bin/networkctl", {"reconfigure", "wlan0"});
    // END : ugly hack
}

int generateList(auto &hostapd, const string &attr, const string &filename)
{
    string value = hostapd.Get(fr::mydatakeeper::ConfigInterface, attr);

    clog << "Writing '" << attr << "' list file" << endl;
    std::ofstream ofs (filename, std::ofstream::out | std::ofstream::trunc);
    ofs << value << endl;
    ofs.close();

    return 0;
}

inline int generateBlacklist(auto &hostapd)
{
    return generateList(hostapd, "deny_mac", config_blacklist);
}

inline int generateWhitelist(auto &hostapd)
{
    return generateList(hostapd, "accept_mac", config_whitelist);
}

bool ieee80211ac;
bool ieee80211h;
bool ieee80211n;
string bandwidth;
string channel;
string country3;
string country_code;
string hw_mode;

string ssid_default;
string passphrase_default;

int generateConfig(auto &hostapd)
{
    bool ieee80211d = hostapd.Get(fr::mydatakeeper::ConfigInterface, "ieee80211d");
    bool wmm_enabled = hostapd.Get(fr::mydatakeeper::ConfigInterface, "wmm_enabled");
    string macaddr_acl = hostapd.Get(fr::mydatakeeper::ConfigInterface, "macaddr_acl");
    string ssid = hostapd.Get(fr::mydatakeeper::ConfigInterface, "ssid");
    string wpa = hostapd.Get(fr::mydatakeeper::ConfigInterface, "wpa");
    string wpa_passphrase = hostapd.Get(fr::mydatakeeper::ConfigInterface, "wpa_passphrase");
    vector<string> rsn_pairwise = hostapd.Get(fr::mydatakeeper::ConfigInterface, "rsn_pairwise");
    vector<string> wpa_key_mgmt = hostapd.Get(fr::mydatakeeper::ConfigInterface, "wpa_key_mgmt");


    generateBlacklist(hostapd);
    generateWhitelist(hostapd);

    clog << "Writing config file" << endl;
    std::ofstream ofs (config_file, std::ofstream::out | std::ofstream::trunc);

    /* Standard */
    ofs << "interface=wlan0" << endl;
    ofs << "bridge=lan0" << endl;
    ofs << "driver=nl80211" << endl;
    ofs << "ctrl_interface=/var/run/hostapd" << endl;
    ofs << "ctrl_interface_group=0" << endl;
    // TODO: retrieve ip from somewhere
    ofs << "own_ip_addr=192.168.10.1" << endl;
    // TODO: set syslog levels
    // ofs << write("logger_syslog=0" << endl;
    // ofs << write("logger_syslog_level=4" << endl;
    // ofs << write("logger_stdout=-1" << endl;
    // ofs << write("logger_stdout_level=0" << endl;

    /* SSID */
    ofs << "utf8_ssid=1" << endl;
    ofs << "ssid2=P\"";
    for (char c: ssid)
        ofs << "\\x" << hex << (uint32_t)c;
    ofs << dec << "\"" << endl;

    /* WPA */
    ofs << "wpa=" << wpa << endl;

    /* Passphrase */
    if (wpa != "0")
        ofs << "wpa_passphrase=" << wpa_passphrase << endl;

    /* Key management algorithms */
    ofs << "wpa_key_mgmt=";
    for (const string &algo: wpa_key_mgmt)
        ofs << algo;
    ofs << endl;

    /* Encryption algorithms */
    ofs << "rsn_pairwise=";
    for (const string &algo: rsn_pairwise)
        ofs << algo;
    ofs << endl;

    /* Wifi multimedia options */
    if (wmm_enabled) {
        ofs << "wmm_enabled=1" << endl;
        ofs << "wmm_ac_bk_cwmin=4" << endl;
        ofs << "wmm_ac_bk_cwmax=10" << endl;
        ofs << "wmm_ac_bk_aifs=7" << endl;
        ofs << "wmm_ac_bk_txop_limit=0" << endl;
        ofs << "wmm_ac_bk_acm=0" << endl;
        ofs << "wmm_ac_be_aifs=3" << endl;
        ofs << "wmm_ac_be_cwmin=4" << endl;
        ofs << "wmm_ac_be_cwmax=10" << endl;
        ofs << "wmm_ac_be_txop_limit=0" << endl;
        ofs << "wmm_ac_be_acm=0" << endl;
        ofs << "wmm_ac_vi_aifs=2" << endl;
        ofs << "wmm_ac_vi_cwmin=3" << endl;
        ofs << "wmm_ac_vi_cwmax=4" << endl;
        ofs << "wmm_ac_vi_txop_limit=94" << endl;
        ofs << "wmm_ac_vi_acm=0" << endl;
        ofs << "wmm_ac_vo_aifs=2" << endl;
        ofs << "wmm_ac_vo_cwmin=2" << endl;
        ofs << "wmm_ac_vo_cwmax=3" << endl;
        ofs << "wmm_ac_vo_txop_limit=47" << endl;
        ofs << "wmm_ac_vo_acm=0" << endl;
    }

    /* IEEE 802.11d : advertise country code */
    ofs << "ieee80211d=" << ieee80211d << endl;

    /* IEEE 802.11h : enable radar detection  &DFS */
    ofs << "ieee80211h=" << ieee80211h << endl;

    /* IEEE 802.11n */
    if (ieee80211n && hw_mode != "b") {
        ofs << "ieee80211n=1" << endl;
        ofs << "ht_capab=";
        for (const string &capa: get_ht_capabilities(hw_mode, bandwidth))
            ofs << capa;
        ofs << endl;
    }

    /* IEEE 802.11ac */
    if (ieee80211ac && hw_mode == "a") {
        ofs << "ieee80211ac=1" << endl;
        ofs << "vht_capab=";
        for (const string &capa: get_vht_capabilities(bandwidth))
            ofs << capa;
        ofs << endl;
        uint8_t chwidth = bandwidth == "160" ? 2 : (bandwidth == "80" ? 1 : 0);
        ofs << "vht_oper_chwidth=" << (uint32_t)chwidth << endl;
        if (bandwidth == "80" || bandwidth == "160")
            ofs << "vht_oper_centr_freq_seg0_idx=" << channel << endl;
    }

    /* Country code */
    ofs << "country_code=" << country_code << endl;

    /* Environment mode */
    ofs << "country3=" << country3 << endl;

    /* Operation mode */
    ofs << "hw_mode=" << hw_mode << endl;

    /* Channel number  &bandwidth */
    uint32_t ch = atoi(channel.c_str());
    if (bandwidth == "20")
        ofs << "channel=" << ch << endl;
    else if (bandwidth == "40")
        ofs << "channel=" << (ch-2) << endl;
    else if (bandwidth == "80")
        ofs << "channel=" << (ch-6) << endl;
    else // if (bandwidth == "160")
        ofs << "channel=" << (ch-14) << endl;

    /* MAC address blacklist/whitelist */
    if (macaddr_acl == "0") {
        ofs << "macaddr_acl=0" << endl;
        ofs << "deny_mac_file=" << config_blacklist << endl;
    } else if (macaddr_acl == "1") {
        ofs << "macaddr_acl=1" << endl;
        ofs << "accept_mac_file=" << config_whitelist << endl;
    }

    ofs.close();

    return 0;
}

inline void update(auto &hostapd)
{
    try {
        if (generateConfig(hostapd) == 0) {
            // Reload the configuration is not good enough.
            // We need to restart the service for the full configuration
            // to be taken into account.
            forkOrRestart();
        }
    } catch (DBus::Error &e) {
        cerr << "An error occured during Hostapd config update : " << e.message() << endl;
    }
}

uint8_t get_channel_number(uint32_t freq)
{
    if (freq == 2484)
        return 14;
    else if (freq < 2484)
        return (freq - 2407) / 5;
    else if (freq >= 4910 && freq <= 4980)
        return (freq - 4000) / 5;
    else if (freq <= 45000) /* DMG band lower limit */
        return (freq - 5000) / 5;
    else if (freq >= 58320 && freq <= 64800)
        return (freq - 56160) / 2160;
    else
        return 0;
}

uint32_t get_frequence(uint8_t channel)
{
    if (channel == 14)
        return 2484;
    else if (channel < 14)
        return (channel * 5) + 2407;
    else if (channel >= 32 && channel <= 173)
        return (channel * 5) + 5000;
    else if (channel >= 183 && channel <= 196)
        return (channel * 5) + 4000;
    else
        return 0;
}

void update_manifest_select(
    auto &hostapd,
    const string &name,
    string &value,
    const auto &/*changed*/,
    const auto &rules,
    auto to_key,
    auto to_value,
    auto get_default,
    auto get_auto_value)
{
    // Retrieve new value
    string new_value = hostapd.Get(fr::mydatakeeper::ConfigInterface, name).operator string();
    bool is_auto = false;

    // Auto detect new value
    if (new_value == "_auto") {
        clog << "Auto detect " << name << endl;
        is_auto = true;
        new_value = get_auto_value();
        if (new_value.empty()) {
            cerr << "Auto detect of " << name << " failed. Using default value instead" << endl;
            new_value = get_default(rules);
        }
    }

    // Validate new value
    const auto& search = find_if(
        rules.begin(), rules.end(), [&to_key, &new_value](const auto& it){
            return to_key(it) == new_value;
        }
    );
    if (search == rules.end()) {
        cerr << "Invalid value for " << name << ". Using default value instead" << endl;
        new_value = get_default(rules);

        // Set value if user defined value is invalid
        // If value is auto detected, don't set
        if (!is_auto) {
            hostapd.Set(fr::mydatakeeper::ConfigInterface, name, to_variant(new_value));
        }
    }

    // Set new value if necessary
    if (new_value != value) {
        value = new_value;

        DBus::Variant v = hostapd.Get(fr::mydatakeeper::ApplicationInterface, "fields");
        map<string,map<string,DBus::Variant>> fields = v;

        map<string, string> old_options = fields[name]["options"];
        map<string, string> options;
        for(const auto &it: rules) {
            auto str_key = to_key(it);
            auto str_value = to_value(it);
            options[str_key] = str_value.empty() ? old_options[str_key] : str_value;
        }
        options["_auto"] = string("Automatique (") + options[value] + ")";

        fields[name]["options"] = to_variant(options);

        hostapd.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));
    }
    clog << "Value of " << name << " is " << value << endl;
}

void update_manifest_boolean(
    const string &name,
    bool &value,
    auto &hostapd,
    const auto &/*changed*/,
    const auto &rules,
    auto get_default,
    auto is_readonly)
{
    // Update fields (will change value if the default is used)
    DBus::Variant v = hostapd.Get(fr::mydatakeeper::ApplicationInterface, "fields");
    map<string,map<string,DBus::Variant>> fields = v;

    fields[name]["default"] = to_variant(get_default(rules));
    fields[name]["readonly"] = to_variant(is_readonly(rules));

    hostapd.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));

    // Retrieve new value
    bool new_value = hostapd.Get(fr::mydatakeeper::ConfigInterface, name).operator bool();

    // Validate value
    const auto& search = find_if(
        rules.begin(), rules.end(), [&new_value](const auto& it) {
            return it.first == new_value;
        }
    );
    if (search == rules.end()) {
        clog << "Invalid value for " << name << ". Using default value instead" << endl;
        new_value = get_default(rules);

        // If the value is not valid, it means the user set it directly.
        // Set the value to the only valid value
        hostapd.Set(fr::mydatakeeper::ConfigInterface, name, to_variant(value));
    }

    if (new_value != value) {
        value = new_value;
    }
    clog << "Value of " << name << " is " << value << endl;
}

void update_wifi_credentials(auto &hostapd)
{
    DBus::Variant v = hostapd.Get(fr::mydatakeeper::ApplicationInterface, "fields");
    map<string,map<string,DBus::Variant>> fields = v;

    // Update default ssid and passphrase
    fields["ssid"]["default"] = to_variant(ssid_default);
    fields["wpa_passphrase"]["default"] = to_variant(passphrase_default);

    hostapd.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));
}

inline void update_manifest(
    auto &hostapd,
    auto &ipapi,
    const map<string, DBus::Variant> &changed)
{
    try {
        update_wifi_credentials(hostapd);

        /* Update country */
        const wireless_rules_t rules = get_wireless_rules();
        update_manifest_select(
            hostapd,
            "country_code",
            country_code,
            changed,
            rules,
            [](const auto &it) { return it.first; },
            [](const auto &/*it*/) { return string(); },
            [](const auto &/*opts*/) { return string("00"); },
            [&ipapi]() { return ipapi.country(); }
        );

        /* Update hw_mode options */
        const auto &rules_1 = rules.at(country_code);
        update_manifest_select(
            hostapd,
            "hw_mode",
            hw_mode,
            changed,
            rules_1,
            [](const auto &it) { return it.first; },
            [](const auto &it) {
                static map<string, string> hw_mode_strings{
                    { "a", "IEEE802.11a - 5GHz" },
                    { "b", "IEEE802.11b - 2.4GHz" },
                    { "g", "IEEE802.11g - 2.4GHz" },
                };
                return hw_mode_strings.at(it.first);
            },
            [](const auto &rules) {
                if (rules.find("a") != rules.end())
                    return string("a");
                if (rules.find("g") != rules.end())
                    return string("g");
                return string("b");
            },
            []() { return string(); }
        );


        /* Update IEEE802.11n options */
        const auto &rules_2 = rules_1.at(hw_mode);
        update_manifest_boolean(
            "ieee80211n",
            ieee80211n,
            hostapd,
            changed,
            rules_2,
            [](const auto &rules) { return rules.rbegin()->first; },
            [](const auto &rules) { return rules.size() == 1; }
        );

        /* Update IEEE802.11ac options */
        const auto &rules_3 = rules_2.at(ieee80211n);
        update_manifest_boolean(
            "ieee80211ac",
            ieee80211ac,
            hostapd,
            changed,
            rules_3,
            [](const auto &rules) { return rules.rbegin()->first; },
            [](const auto &rules) { return rules.size() == 1; }
        );

        /* Update bandwidth options */
        const auto &rules_4 = rules_3.at(ieee80211ac);
        update_manifest_select(
            hostapd,
            "bandwidth",
            bandwidth,
            changed,
            rules_4,
            [](const auto &it) { return it.first; },
            [](const auto &it) { return it.first + "MHz"; },
            [](const auto &rules) {
                if (rules.find("160") != rules.end())
                    return string("160");
                if (rules.find("80+80") != rules.end())
                    return string("80+80");
                if (rules.find("80") != rules.end())
                    return string("80");
                if (rules.find("40") != rules.end())
                    return string("40");
                return string("20");
            },
            []() { return string(); }
        );

        /* Update country3 options */
        const auto &rules_5 = rules_4.at(bandwidth);
        update_manifest_select(
            hostapd,
            "country3",
            country3,
            changed,
            rules_5,
            [](const auto &it) { return it.first; },
            [](const auto &it) {
                static map<string, string> country3_strings{
                    { "0x4f", "Exterieur" },
                    { "0x49", "Interieur" },
                };
                return country3_strings.at(it.first);
            },
            [](const auto &rules) {
                if (rules.find("0x49") != rules.end())
                    return string("0x49");
                return string("0x4f");
            },
            []() { return string(); }
        );

        /* Update IEEE802.11h options */
        const auto &rules_6 = rules_5.at(country3);
        update_manifest_boolean(
            "ieee80211h",
            ieee80211h,
            hostapd,
            changed,
            rules_6,
            [](const auto &rules) { return rules.rbegin()->first; },
            [](const auto &rules) { return rules.size() == 1; }
        );

        /* Update channel options */
        const auto &rules_7 = rules_6.at(ieee80211h);
        auto to_channel_string = [](const auto &freq) {
            string str = to_string(get_channel_number(freq));
            if (str.size() < 3)
                str.insert(0, 3 - str.size(), '0');
            return str;
        };
        update_manifest_select(
            hostapd,
            "channel",
            channel,
            changed,
            rules_7,
            to_channel_string,
            [](const auto &freq) {
                return to_string(get_channel_number(freq)) + " - " + to_string(freq) + "MHz";
            },
            [&to_channel_string](const auto &rules) {
                return to_channel_string(rules.front());
            },
            []() { return string(); }
        );
    } catch (DBus::Error &e) {
        cerr << "An error occured during Hostapd manifest update : " << e.message() << endl;
    }

    update(hostapd);
}

void handler(int sig)
{
    clog << "Stopping DBus main loop (sig " << sig << ')' << endl;
    dispatcher.leave();
    killIfForked();
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, conf_dir, script_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/app/access_x45_point", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            conf_dir, "conf-dir", 0, mydatakeeper::required_argument, "/var/lib/mydatakeeper-config/apps/fr.mydatakeeper.app.access-point", nullptr,
            "The folder where configuration files will be generated"
        },
        {
            script_dir, "script-dir", 0, mydatakeeper::required_argument, "/usr/lib/mydatakeeper-access-point", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    pid = 0;

    config_file = conf_dir + "/hostapd.conf";
    config_blacklist = conf_dir + "/blacklist.txt";
    config_whitelist = conf_dir + "/whitelist.txt";

    string err;
    if (execute(script_dir + "/default-ssid", {}, &ssid_default, &err) != 0) {
        cerr << script_dir << "/default-ssid: " << err << endl;
        return -1;
    }
    if (execute(script_dir  + "/default-passphrase", {}, &passphrase_default, &err) != 0) {
        cerr << script_dir << "/default-passphrase: " << err << endl;
        return -1;
    }
    clog << "Configuring access point with "
         << "SSID: " << ssid_default << " "
         << "passphrase: " << passphrase_default << endl;

    clog << "Getting Ipapi Proxy" << endl;
    IpapiProxy ipapi(conn);

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy access_point(conn, bus_path, bus_name);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    access_point.onPropertiesChanged = [&access_point, &ipapi](auto &/*iface*/, auto &changed, auto &/*invalidated*/)
    {
        clog << "Access point config has changed" << endl;
        update_manifest(access_point, ipapi, changed);
    };

    clog << "Setting up Ipapi update listener" << endl;
    ipapi.onPropertiesChanged = [&access_point, &ipapi](auto &/*iface*/, auto &changed, auto &/*invalidated*/)
    {
        if (changed.find("country") == changed.end())
            return;

        clog << "Ipapi country has changed" << endl;
        update_manifest(access_point, ipapi, {});
    };

    // Force update during init
    update_manifest(access_point, ipapi, {});

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}