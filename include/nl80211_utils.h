#ifndef NL80211_UTILS_H_INCLUDED
#define NL80211_UTILS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

struct nl80211_phy_band {
    uint8_t has_ht_cap;
    uint16_t ht_cap;
    uint8_t has_vht_cap;
    uint32_t vht_cap;
    size_t freq_count;
    uint32_t *freqs;
};

struct nl80211_phy {
    uint32_t id;
    char* name;
    size_t band_count;
    struct nl80211_phy_band *bands;
};

int get_phy_description(signed long long idx, struct nl80211_phy *phy);

#ifdef __cplusplus
}
#endif

#endif /* NL80211_UTILS_H_INCLUDED */
