#ifndef REGDB_UTILS_H_INCLUDED
#define REGDB_UTILS_H_INCLUDED

#include <list>
#include <map>
#include <set>
#include <string>

#define REGBD_FILENAME "/usr/lib/crda/regulatory.bin"

#define BIT(x) (1U << (x))

#define HT_CAP_LDPC             ((uint16_t) BIT(0))
#define HT_CAP_CHANNEL_WIDTH    ((uint16_t) BIT(1))
#define HT_CAP_SMPS_MASK        ((uint16_t) (BIT(2) | BIT(3)))
#define HT_CAP_SMPS_STATIC      ((uint16_t) 0)
#define HT_CAP_SMPS_DYNAMIC     ((uint16_t) BIT(2))
#define HT_CAP_SMPS_DISABLED    ((uint16_t) (BIT(2) | BIT(3)))
#define HT_CAP_GREEN_FIELD      ((uint16_t) BIT(4))
#define HT_CAP_SHORT_GI20MHZ    ((uint16_t) BIT(5))
#define HT_CAP_SHORT_GI40MHZ    ((uint16_t) BIT(6))
#define HT_CAP_TX_STBC          ((uint16_t) BIT(7))
#define HT_CAP_RX_STBC_MASK     ((uint16_t) (BIT(8) | BIT(9)))
#define HT_CAP_RX_STBC_1        ((uint16_t) BIT(8))
#define HT_CAP_RX_STBC_12       ((uint16_t) BIT(9))
#define HT_CAP_RX_STBC_123      ((uint16_t) (BIT(8) | BIT(9)))
#define HT_CAP_DELAYED_BA       ((uint16_t) BIT(10))
#define HT_CAP_MAX_AMSDU_7935   ((uint16_t) BIT(11))
#define HT_CAP_DSSS_CCK40MHZ    ((uint16_t) BIT(12))
#define HT_CAP_40MHZ_INTOLERANT ((uint16_t) BIT(14))
#define HT_CAP_LSIG_TXOP_PROT   ((uint16_t) BIT(15))

#define VHT_CAP_MAX_MPDU_LENGTH_7991                ((uint32_t) BIT(0))
#define VHT_CAP_MAX_MPDU_LENGTH_11454               ((uint32_t) BIT(1))
#define VHT_CAP_MAX_MPDU_LENGTH_MASK                ((uint32_t) BIT(0) | BIT(1))
#define VHT_CAP_MAX_MPDU_LENGTH_MASK_SHIFT          0
#define VHT_CAP_SUPP_CHAN_WIDTH_160MHZ              ((uint32_t) BIT(2))
#define VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ     ((uint32_t) BIT(3))
#define VHT_CAP_SUPP_CHAN_WIDTH_MASK                ((uint32_t) BIT(2) | BIT(3))
#define VHT_CAP_SUPP_CHAN_WIDTH_MASK_SHIFT          2
#define VHT_CAP_RXLDPC                              ((uint32_t) BIT(4))
#define VHT_CAP_SHORT_GI_80                         ((uint32_t) BIT(5))
#define VHT_CAP_SHORT_GI_160                        ((uint32_t) BIT(6))
#define VHT_CAP_TXSTBC                              ((uint32_t) BIT(7))
#define VHT_CAP_RXSTBC_1                            ((uint32_t) BIT(8))
#define VHT_CAP_RXSTBC_2                            ((uint32_t) BIT(9))
#define VHT_CAP_RXSTBC_3                            ((uint32_t) BIT(8) | BIT(9))
#define VHT_CAP_RXSTBC_4                            ((uint32_t) BIT(10))
#define VHT_CAP_RXSTBC_MASK                         ((uint32_t) BIT(8) | BIT(9) | BIT(10))
#define VHT_CAP_RXSTBC_MASK_SHIFT                   8
#define VHT_CAP_SU_BEAMFORMER_CAPABLE               ((uint32_t) BIT(11))
#define VHT_CAP_SU_BEAMFORMEE_CAPABLE               ((uint32_t) BIT(12))
#define VHT_CAP_BEAMFORMEE_STS_2                    ((uint32_t) BIT(13))
#define VHT_CAP_BEAMFORMEE_STS_3                    ((uint32_t) BIT(14))
#define VHT_CAP_BEAMFORMEE_STS_4                    ((uint32_t) BIT(15))
#define VHT_CAP_SOUNDING_DIMENSION_2                ((uint32_t) BIT(16))
#define VHT_CAP_SOUNDING_DIMENSION_3                ((uint32_t) BIT(17))
#define VHT_CAP_SOUNDING_DIMENSION_4                ((uint32_t) BIT(18))
#define VHT_CAP_MU_BEAMFORMER_CAPABLE               ((uint32_t) BIT(19))
#define VHT_CAP_MU_BEAMFORMEE_CAPABLE               ((uint32_t) BIT(20))
#define VHT_CAP_VHT_TXOP_PS                         ((uint32_t) BIT(21))
#define VHT_CAP_HTC_VHT                             ((uint32_t) BIT(22))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_1        ((uint32_t) BIT(23))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_2        ((uint32_t) BIT(24))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_3        ((uint32_t) BIT(23) | BIT(24))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_4        ((uint32_t) BIT(25))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_5        ((uint32_t) BIT(23) | BIT(25))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_6        ((uint32_t) BIT(24) | BIT(25))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX      ((uint32_t) BIT(23) | BIT(24) | BIT(25))
#define VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX_SHIFT 23
#define VHT_CAP_VHT_LINK_ADAPTATION_VHT_UNSOL_MFB   ((uint32_t) BIT(27))
#define VHT_CAP_VHT_LINK_ADAPTATION_VHT_MRQ_MFB     ((uint32_t) BIT(26) | BIT(27))
#define VHT_CAP_RX_ANTENNA_PATTERN                  ((uint32_t) BIT(28))
#define VHT_CAP_TX_ANTENNA_PATTERN                  ((uint32_t) BIT(29))

using namespace std;

// Types of indexes
typedef string   country_t;
typedef string   hw_mode_t;
typedef bool     ieee80211n_t;
typedef bool     ieee80211ac_t;
typedef string   bandwidth_t;
typedef string   country3_t;
typedef bool     ieee80211h_t;
typedef uint32_t freq_t;

// Type of rules
typedef list<freq_t>                                        freqs_t;
typedef map<ieee80211h_t,  freqs_t>                         freqs_by_dfs_t;
typedef map<country3_t,    freqs_by_dfs_t>                  freqs_by_env_dfs_t;
typedef map<bandwidth_t,   freqs_by_env_dfs_t>              freqs_by_bw_env_dfs_t;
typedef map<ieee80211ac_t, freqs_by_bw_env_dfs_t>           freqs_by_ac_bw_env_dfs_t;
typedef map<ieee80211n_t,  freqs_by_ac_bw_env_dfs_t>        freqs_by_n_ac_bw_env_dfs_t;
typedef map<hw_mode_t,     freqs_by_n_ac_bw_env_dfs_t>      freqs_by_mode_n_ac_bw_env_dfs_t;
typedef map<country_t,     freqs_by_mode_n_ac_bw_env_dfs_t> freqs_by_country_mode_n_ac_bw_env_dfs_t;

typedef freqs_by_country_mode_n_ac_bw_env_dfs_t wireless_rules_t;

set<string> get_ht_capabilities(const hw_mode_t &mode, const bandwidth_t &bandwidth);
set<string> get_vht_capabilities(const bandwidth_t &bandwidth);

wireless_rules_t& get_wireless_rules();

#endif /* REGDB_UTILS_H_INCLUDED */
