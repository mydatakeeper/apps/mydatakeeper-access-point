#include "nl80211_utils.h"

#include <stdlib.h>

#define HT_CAP_40MHZ_CHANNEL_WIDTH 0x2

int get_phy_description(signed long long idx, struct nl80211_phy *phy)
{
    static uint32_t freqs_2ghz[14] = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484,};
    static uint32_t freqs_5ghz[29] = {5170, 5180, 5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500, 5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5720, 5745, 5765, 5785, 5805, 5825,};

    phy->id = 0;
    phy->name = (char*)"wlan0";
    phy->band_count = 2;
    phy->bands = (struct nl80211_phy_band*)malloc(sizeof(struct nl80211_phy_band) * 2);

    phy->bands[0].has_ht_cap = 1;
    phy->bands[0].ht_cap = HT_CAP_40MHZ_CHANNEL_WIDTH;
    phy->bands[0].has_vht_cap = 0;
    phy->bands[0].vht_cap = 0;
    phy->bands[0].freq_count = 14;
    phy->bands[0].freqs = freqs_2ghz;

    phy->bands[1].has_ht_cap = 1;
    phy->bands[1].ht_cap = HT_CAP_40MHZ_CHANNEL_WIDTH;
    phy->bands[1].has_vht_cap = 1;
    phy->bands[1].vht_cap = 0x1020;
    phy->bands[1].freq_count = 29;
    phy->bands[1].freqs = freqs_5ghz;

    return 0;
}
